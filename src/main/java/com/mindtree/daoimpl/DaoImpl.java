package com.mindtree.daoimpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.dao.Dao;
import com.mindtree.entity.AddSong;
@Component
@Repository
public class DaoImpl implements Dao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public void addSong(AddSong mycontroller) {
		
		Session session = sessionFactory.openSession();
		System.out.println("add song called");
		Transaction transaction = session.beginTransaction();
		
		session.save(mycontroller);
		transaction.commit();
		
	}

	@Override
	@Transactional
	public List<AddSong> listSong() {
		
		System.out.println("listSong called");
		Session session = this.sessionFactory.getCurrentSession();
		
		List<AddSong> songList = session.createQuery("from AddSong").list();
		/*for(AddSong addsong : songList)
		{
			System.out.println("Song Lists: "+addsong);
		}*/
		return songList;
	}
}
