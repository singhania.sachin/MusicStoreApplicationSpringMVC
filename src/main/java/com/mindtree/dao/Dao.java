package com.mindtree.dao;

import java.util.List;

import com.mindtree.entity.AddSong;

public interface Dao {

	public void addSong(AddSong mycontroller);

	public List<AddSong> listSong();
}
