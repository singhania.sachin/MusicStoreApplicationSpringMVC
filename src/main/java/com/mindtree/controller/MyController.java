package com.mindtree.controller;

import java.io.IOException;
import java.util.List;

import org.hibernate.annotations.OnDelete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.entity.AddSong;
import com.mindtree.service.SongService;

@Controller
public class MyController {

	@Autowired
	private SongService songservice;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	protected ModelAndView first() {
		ModelAndView modelandview = new ModelAndView("index");
		return modelandview;
	}

	@RequestMapping(value = "/addSong", method = RequestMethod.GET)
	public ModelAndView addSongHere() {
		
		ModelAndView modelandview = new ModelAndView("addSong");
		return modelandview;
	}

	@RequestMapping(value = "/submit",method = RequestMethod.POST)
	public ModelAndView submitSong(@ModelAttribute AddSong mycontroller) {
		songservice.addSong(mycontroller);
		ModelAndView modelandview = new ModelAndView("submitted");
		return modelandview;
	}
	
	@RequestMapping(value="/searchSong")
	public ModelAndView listSong(ModelAndView modelandview) throws IOException	
	{
		
		List<AddSong> listSong=songservice.listSong();
		/*for(AddSong addsong : songList)
		{
			System.out.println("Song Lists: "+addsong);
		}*/
		modelandview.addObject("listSong", listSong);
		modelandview.setViewName("searchSong");
		return modelandview;
		
	}

}
