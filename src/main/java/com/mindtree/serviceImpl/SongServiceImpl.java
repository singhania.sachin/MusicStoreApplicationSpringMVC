package com.mindtree.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.controller.MyController;
import com.mindtree.dao.Dao;
import com.mindtree.entity.AddSong;
import com.mindtree.service.SongService;

@Component
@Repository
public class SongServiceImpl implements SongService {

	@Autowired
	private Dao dao;
	
	@Override
	@Transactional
	public void addSong(AddSong mycontroller) {
		
		dao.addSong(mycontroller);
	}

	@Override
	@Transactional
	public List<AddSong> listSong() {
	
		return this.dao.listSong();
	}

}
