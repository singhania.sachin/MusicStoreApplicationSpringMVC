package com.mindtree.service;

import java.util.List;

import com.mindtree.entity.AddSong;

public interface SongService {
	
	public void addSong(AddSong mycontroller);
	
	public List<AddSong> listSong();

}
